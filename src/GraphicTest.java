import org.antlr.v4.gui.TreeViewer;
import throwable.*;
import parser.SVMLexer;
import parser.SVMParser;
import sem_util.SymbolTable;
import type.Type;
import util.FoolVisitorImpl;
import ast.Node;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import parser.FOOLLexer;
import parser.FOOLParser;
import vm_util.DispatchTable;
import virtual_machine.ExecuteVM;

import javax.swing.*;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;


public class GraphicTest {
    public static void main(String[] args)  {

        try { //RILEVAZIONE INPUT
            System.out.println("Rilevazione Input...\n");
            String fileName = "src/prova.fool";
            CharStream input = CharStreams.fromFileName(fileName);

            System.out.println("Analisi Lessicale...\n");
            FOOLLexer lexer = new FOOLLexer(input);
            CommonTokenStream tokens = new CommonTokenStream(lexer);

            if (lexer.lexicalErrors.size() > 0){
                throw new LexerException(lexer.lexicalErrors);
            }

            System.out.println("Analisi Sintattica...\n");
            FOOLParser parser = new FOOLParser(tokens);
            FOOLParser.ProgContext progContext = parser.prog(); //parser.prog riutilizzato

            if (parser.getNumberOfSyntaxErrors() > 0){
                throw new ParserException("Errori rilevati: " + parser.getNumberOfSyntaxErrors() + "\n");
            }

            ParseTree tree = progContext;

            System.out.println("Sto per visualizzare l'AST...\n");
            //show AST in console
            //System.out.println(tree.toStringTree(parser) + "\n");

            //show AST in GUI
            JFrame frame = new JFrame("Antlr AST");
            JPanel panel = new JPanel();
            TreeViewer viewr = new TreeViewer(Arrays.asList(
                    parser.getRuleNames()), tree);
            viewr.setScale(1.5);
            panel.add(viewr);
            frame.add(panel);
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setSize(1500, 700);
            frame.setVisible(true);

            System.out.println("Analisi Semantica...\n");

            FoolVisitorImpl visitor = new FoolVisitorImpl();

            Node ast = visitor.visit(progContext); //generazione AST

            SymbolTable env = new SymbolTable();
            ArrayList<String> err = ast.checkSemantics(env);

            if (err.size() > 0) {
                throw new SemanticException(err);
            }

            Type type = ast.typeCheck(); //type-checking bottom-up
            System.out.println("Type checking ok! Il tipo del programma è: " + type.toPrint() + "\n");

            // CODE GENERATION
            String code = ast.codeGeneration();
            code += DispatchTable.generaCodiceDispatchTable();

            File svmFile = new File("codice.svm");
            BufferedWriter svmWriter = new BufferedWriter(new FileWriter(svmFile.getAbsoluteFile()));
            svmWriter.write(code);
            svmWriter.close();

            System.out.println("Codice SVM generato: (" + code.split("\n").length + " linee). Output visibile in codice.svm. \n");

            //Scommenta se vuoi vedere l'output del codice a console
            //System.out.println(code);

            CharStream inputASM = CharStreams.fromFileName(svmFile.getAbsolutePath());
            SVMLexer lexerASM = new SVMLexer(inputASM);
            CommonTokenStream tokensASM = new CommonTokenStream(lexerASM);
            SVMParser parserASM = new SVMParser(tokensASM);

            parserASM.assembly();

            if (lexerASM.errors.size() > 0) {
                throw new LexerException(lexerASM.errors);
            }
            if (parserASM.getNumberOfSyntaxErrors() > 0) {
                throw new ParserException("Errore di parsing in SVM");
            }
            int[] bytecode= parserASM.getBytecode();
            ExecuteVM vm = new ExecuteVM(bytecode);
            String risultato = "No output";
            ArrayList<String> out = vm.cpu();
            if (out.size() > 0)
                risultato = out.get(out.size() - 1);
            System.out.println("Risultato: "+risultato+"\n");
        }
        catch (LexerException | IOException | SemanticException | TypeException | ParserException e) {
            System.out.println(e.getMessage());
        }
    }
}
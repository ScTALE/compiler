package ast;

import throwable.TypeException;
import throwable.UndecIDException;
import org.antlr.v4.runtime.ParserRuleContext;
import sem_util.SymbolTable;
import sem_util.STentry;
import type.ArrowType;
import type.ObjectType;
import type.Type;

import java.util.ArrayList;

public class IdNode implements Node {

    private String ID;
    private STentry entry;
    private int nestingLevel;
    private ParserRuleContext parserRuleContext;

    //variabili utilizzate da this
    private int thisNestingLevel;
    private int thisOffset;

    //variabile utilizzata per gestire il meno davanti agli ID
    private boolean isNegative;

    public IdNode(String i, ParserRuleContext context,boolean isNeg) {
        parserRuleContext = context;
        ID = i;
        isNegative=isNeg;
    }

    @Override
    public ArrayList<String> checkSemantics(SymbolTable env) {
        //cercare ID nella symbol table, con casi particolari per le classi
        ArrayList<String> res = new ArrayList<>();

        try {
            //è utilizzato per poter definire un oggetto con lo stesso nome di un metodo
            //all'interno di una classe
            entry = env.processUseIgnoreArrow(ID);
            //getInsideClass è true quando si è dentro ad una classe
            if(entry.getInsideClass()) {
                //utilizzato per conoscere il nestingLevel e offset di this
                STentry thisPointer = env.processUse("this");
                thisNestingLevel = thisPointer.getNestinglevel();
                thisOffset = thisPointer.getOffset();
            }
            nestingLevel = env.getNestingLevel();

            //serve per assegnare il supertipo dinamicamente agli oggetti
            //vedi NewTest
            if (entry.getType() instanceof ObjectType) {
                ObjectType decType = (ObjectType) entry.getType();
                res.addAll(decType.updateClassType(env));
            }

        } catch (UndecIDException e) {
            res.add(ID + ": identificativo non definito");
        }

        return res;
    }

    @Override
    public Type typeCheck() throws TypeException {
        if (entry.getType() instanceof ArrowType) {
            throw new TypeException("Utilizzo errato di identificativo di funzione", parserRuleContext);
        }
        return entry.getType();
    }

    public String codeGeneration() {
        StringBuilder getActivationRecord = new StringBuilder();
        if(entry.getInsideClass()) {
            //for e getActivationRecord per gestire le funzioni annidate
            for (int i = 0; i < nestingLevel - thisNestingLevel; i++)
                getActivationRecord.append("lw\n");
            if(isNegative){
                //caso in cui l'espressione cominci con un meno
                return  "push " + entry.getOffset() + "\n" + // pusho offset dell'ID
                        "push " + thisOffset + "\n" + //pusho offset di this
                        "lfp\n" + getActivationRecord +
                        "add\n" +
                        "lw\n" +
                        "heapoffset\n" +  // converto l'offset logico nell'offset fisico a cui l'identificatore
                                          // si riferisce, poi lo carica sullo stack
                                          // utilizzato solo per i parametri dei metodi all'interno
                                          // delle classi
                        "add\n" +
                        "lw\n"+ //carico sullo stack il valore all'indirizzo ottenuto
                        "push -1\n"+
                        "mult\n";
            }
            else return  "push " + entry.getOffset() + "\n" + // pusho offset dell'ID
                    "push " + thisOffset + "\n" +   //pusho offset di this
                    "lfp\n" + getActivationRecord +
                    "add\n" +
                    "lw\n" +
                    "heapoffset\n" +  // converto l'offset logico nell'offset fisico a cui l'identificatore
                                      // si riferisce, poi lo carica sullo stack
                                      // utilizzato solo per i parametri dei metodi all'interno
                                      // delle classi
                    "add\n" +
                    "lw\n"; //carico sullo stack il valore all'indirizzo ottenuto
        } else {
            //for e getActivationRecord per gestire le funzioni annidate
            for (int i = 0; i < nestingLevel - entry.getNestinglevel(); i++)
                getActivationRecord.append("lw\n");
            if(isNegative) {
                return "push " + entry.getOffset() + "\n" + //metto offset sullo stack
                        "lfp\n" + getActivationRecord + //risalgo la catena statica
                        "add\n" +
                        "lw\n"+ //carico sullo stack il valore all'indirizzo ottenuto
                        "push -1\n"+
                        "mult\n";
            }
            else return "push " + entry.getOffset() + "\n" + //metto offset sullo stack
                    "lfp\n" + getActivationRecord + //risalgo la catena statica
                    "add\n" +
                    "lw\n"; //carico sullo stack il valore all'indirizzo ottenuto
        }
    }
}
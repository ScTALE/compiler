package ast;

import throwable.MultipleIDException;
import throwable.TypeException;
import sem_util.*;
import type.ArrowType;
import type.ClassType;
import type.Type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.ListIterator;

public class ClassDecNode implements Node {

    //ArrayList contenente tutte le dichiarazioni di classe
    private ArrayList<ClassNode> classDeclarationsArrayList;
    private LetInNode letInNode;

    public ClassDecNode(ArrayList<ClassNode> cd, LetInNode li) {
        classDeclarationsArrayList = cd;
        letInNode=li;
    }

    @Override
    public ArrayList<String> checkSemantics(SymbolTable env) {
        //res è l'Arraylist contenente gli eventuali errori
        ArrayList<String> res = new ArrayList<>();

        //crea nuovo livello di scope
        HashMap<String, STentry> hm = new HashMap<>();
        env.pushHashMap(hm);

        for (ClassNode classNode : classDeclarationsArrayList) {
            try {

                //defisco la classe con tutti i suoi campi e metodi

                ArrayList<Field> fieldArrayList = new ArrayList<>();
                ArrayList<Method> methodArrayList = new ArrayList<>();

                for (ParameterNode parameterNode : classNode.getFieldDeclarationArrayList()) {
                    fieldArrayList.add(new Field(parameterNode.getID(), parameterNode.getType()));
                }

                for (MethodNode methodNode : classNode.getMethodDeclarationArrayList()) {
                    ArrayList<Type> parameterTypeArrayList = new ArrayList<>();
                    for (ParameterNode parameterNode : methodNode.getParameterNodeArrayList()) {
                        parameterTypeArrayList.add(parameterNode.getType());
                    }
                    methodArrayList.add(new Method(methodNode.getID(), new ArrowType(parameterTypeArrayList, methodNode.getReturnType())));
                }

                //controllo se la classe è già stata definita
                ClassType classType = new ClassType(classNode.getClassID(), new ClassType(classNode.getSuperClassID()), fieldArrayList, methodArrayList);
                env.processDeclaration(classNode.getClassID(), classType, 0);
            } catch (MultipleIDException e) {
                res.add("La classe '" + classNode.getClassID() + "' è dichiarata più volte");
            }
        }

        //checkSemantic su ogni classe dichiarata
        for (ClassNode classNode : classDeclarationsArrayList) {
            res.addAll(classNode.checkSemantics(env));
        }

        //checksemantic sul letInNode
        res.addAll(letInNode.checkSemantics(env));

        //esco dal livello di scope
        env.popHashMap();

        return res;
    }

    @Override
    public Type typeCheck() throws TypeException {
        for (ClassNode classNode : classDeclarationsArrayList) {
            classNode.typeCheck();
        }

        return letInNode.typeCheck();
    }

    @Override
    public String codeGeneration() {

        ArrayList<ClassNode> classNodeArrayList = new ArrayList<>();
        HashMap<String, ClassNode> classHashMap = new HashMap<>();
        String nameDeclaration = "";

        //questi due loop sono usati per ordinare la dichiarazioni di classi in ordine top-down
        //per gestire superclassi e sottoclassi
        ListIterator iterator = classDeclarationsArrayList.listIterator();
        while(iterator.hasNext()){
            ClassNode classDec = (ClassNode) iterator.next();
            //se la classe che sto esaminando non ha una superclasse allora:
            if (classDec.getSuperClassID() == null || classDec.getSuperClassID().isEmpty()){
                //aggiungo alla lista di classi la dichiarazione della classe
                classNodeArrayList.add(classDec);
                //aggiungo alla hashmap l'identificatore della classe e la dichiarazione della classe
                //serve nel ciclo successivo per ottenere la superclasse
                classHashMap.put(classDec.getClassID(), classDec);
                //per scorrere
                iterator.remove();
            }
        }
        //se entro dentro a questo while, vuole dire che è presente almeno una classe che ne estende un'altra
        while (classDeclarationsArrayList.size() != 0) {
            //per ripartire dall'inizio
            iterator = classDeclarationsArrayList.listIterator();
            while(iterator.hasNext()){
                //contiene la classe sottoclasse
                ClassNode subClass = (ClassNode) iterator.next();
                //contiene l'identificatore della superclasse
                String superClassName = subClass.getSuperClassID();
                //contiene la superclasse
                ClassNode superClass = classHashMap.get(superClassName);
                //nel caso si estenda una classe ancora non dichiarata si entra in questo if
                if (superClass != null){
                    //aggiungo alle classi dichiarati la sottoclasse
                    classNodeArrayList.add(subClass);
                    //aggiungo alla hashmap l'identificatore della sottoclasse e la sua dichiarazione
                    classHashMap.put(subClass.getClassID(), subClass);
                    iterator.remove();
                }

            }
        }

        //per ogni classe dichiarata, eseguo la code generation
        for (ClassNode cl : classNodeArrayList) {
            nameDeclaration = nameDeclaration + cl.codeGeneration();
        }

        return nameDeclaration + letInNode.codeGeneration();
    }
}
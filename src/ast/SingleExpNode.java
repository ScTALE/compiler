package ast;

import throwable.TypeException;
import sem_util.SymbolTable;
import type.Type;

import java.util.ArrayList;

public class SingleExpNode implements Node {
    private Node expression;

    public SingleExpNode(Node exp) {
        expression = exp;
    }

    @Override
    public ArrayList<String> checkSemantics(SymbolTable env) {
        return expression.checkSemantics(env);
    }

    @Override
    public Type typeCheck() throws TypeException {
        return expression.typeCheck();
    }

    @Override
    public String codeGeneration() {
        return expression.codeGeneration() + "halt\n";
    }
}

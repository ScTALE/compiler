package ast;

import throwable.TypeException;
import parser.FOOLParser;
import sem_util.SymbolTable;
import type.BoolType;
import type.Type;
import vm_util.Label;

import java.util.ArrayList;

public class IfNode implements Node {
    private Node conditionNode;
    private Node thenNode;
    private Node elseNode;
    private FOOLParser.IfExpContext ifExpContext;

    public IfNode(Node cond, Node th, Node el,FOOLParser.IfExpContext context) {
        conditionNode = cond;
        thenNode = th;
        elseNode = el;
        ifExpContext = context;
    }

    @Override
    public ArrayList<String> checkSemantics(SymbolTable env) {

        ArrayList<String> res = new ArrayList<>();

        //checkSemantic sulla condizione
        res.addAll(conditionNode.checkSemantics(env));

        //checkSemantic sui rami then ed else
        res.addAll(thenNode.checkSemantics(env));
        res.addAll(elseNode.checkSemantics(env));

        return res;
    }

    @Override
    public Type typeCheck() throws TypeException {
        if (!conditionNode.typeCheck().isSubType(new BoolType())) {
            throw new TypeException("La condizione dell'if non è booleana", ifExpContext);
        }
        Type thenType = thenNode.typeCheck();
        Type elseType = elseNode.typeCheck();
        if (thenType.isSubType(elseType)) {
            return elseType;
        } else if (elseType.isSubType(thenType)) {
            return thenType;
        } else {
            throw new TypeException("Tipi incompatibili nei rami then ed else", ifExpContext);
        }
    }

    @Override
    public String codeGeneration() {
        String thenBranch = Label.nuovaLabel();
        String exit = Label.nuovaLabel();
        return conditionNode.codeGeneration() +
                "push 1\n" +
                "beq " + thenBranch + "\n" +
                elseNode.codeGeneration() +
                "b " + exit + "\n" +
                thenBranch + ":\n" +
                thenNode.codeGeneration() +
                exit + ":\n";
    }
}

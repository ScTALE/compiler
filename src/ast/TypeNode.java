package ast;

import throwable.TypeException;
import throwable.UndecIDException;
import sem_util.SymbolTable;
import type.*;

import java.util.ArrayList;

public class TypeNode implements Node {

    String assignedType;
    Type type;

    public TypeNode(String type) {
        assignedType = type;
        switch (type) {
            case "int":
                this.type = new IntType();
                break;
            case "bool":
                this.type = new BoolType();
                break;
            default:
                this.type = new ObjectType(new ClassType(assignedType));
                break;
        }
    }

    @Override
    public ArrayList<String> checkSemantics(SymbolTable env) {
        ArrayList<String> res = new ArrayList<>();
        try {
            this.type = env.processUse(assignedType).getType();
        } catch (UndecIDException e) {
            res.add("La classe '" + assignedType + "' non esiste");
        }
        return res;
    }

    @Override
    public Type typeCheck() throws TypeException {
        return type;
    }

    @Override
    public String codeGeneration() {
        return "";
    }
}


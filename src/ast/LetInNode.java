package ast;

import throwable.TypeException;
import sem_util.SymbolTable;
import type.Type;
import sem_util.STentry;
import vm_util.FunctionCode;

import java.util.ArrayList;
import java.util.HashMap;

public class LetInNode implements Node {
    private ArrayList<Node> declarationArrayList;
    private Node expression;

    public LetInNode(ArrayList<Node> dec,Node e) {
        declarationArrayList = dec;
        expression = e;
    }

    @Override
    public ArrayList<String> checkSemantics(SymbolTable env) {
        ArrayList<String> res = new ArrayList<>();

        HashMap<String, STentry> hashMap = new HashMap<>();
        //entro in un nuovo livello di scope
        env.pushHashMap(hashMap);
        //parte Let
        //CheckSemantic nella lista di dichiarazioni
        if (declarationArrayList.size() > 0)
        {
            env.setOffset(-2);
            //Checksemantic nei figli
            for (Node n : declarationArrayList)
                res.addAll(n.checkSemantics(env));
        }

        //Parte In
        res.addAll(expression.checkSemantics(env));

        //lascio il vecchio scope
        env.popHashMap();

        return res;
    }

    @Override
    public Type typeCheck() throws TypeException {
        //parte let
        for (Node dec : declarationArrayList)
            dec.typeCheck();
        //parte in
        return expression.typeCheck();
    }

    @Override
    public String codeGeneration() {
        StringBuilder declCode = new StringBuilder();
        for (Node dec : declarationArrayList)
            declCode.append(dec.codeGeneration());
        return "push 0\n" +
                declCode +
                expression.codeGeneration() + "halt\n" +
                FunctionCode.getFunctionsCode();
    }
}
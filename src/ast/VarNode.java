package ast;

import throwable.MultipleIDException;
import throwable.TypeException;
import parser.FOOLParser;
import sem_util.SymbolTable;
import type.ObjectType;
import type.Type;

import java.util.ArrayList;

public class VarNode implements Node {

    private String ID;
    private Type assignedType;
    private Node expression;
    private FOOLParser.VarasmContext varasmContext;

    public VarNode(String i, Type t, Node assignedExp, FOOLParser.VarasmContext context) {
        ID = i;
        assignedType = t;
        expression = assignedExp;
        varasmContext = context;
    }

    public String getID() {
        return ID;
    }

    @Override
    public ArrayList<String> checkSemantics(SymbolTable env) {

        ArrayList<String> res = new ArrayList<>();

        //Se sto instanziando un nuovo oggetto, aggiorno le informazioni
        if (assignedType instanceof ObjectType) {
            ObjectType decType = (ObjectType) assignedType;
            res.addAll(decType.updateClassType(env));
        }

        res.addAll(expression.checkSemantics(env));

        try {
            env.processDeclaration(ID, assignedType, env.getOffset());
            env.decreaseOffset();
        } catch (MultipleIDException e) {
            res.add(e.getMessage());
        }

        return res;
    }

    @Override
    public Type typeCheck() throws TypeException {
        if (!expression.typeCheck().isSubType(assignedType)) {
            throw new TypeException("Valore incompatibile per la variabile " + ID, varasmContext.exp());
        }

        return assignedType;
    }

    @Override
    public String codeGeneration() {
        return expression.codeGeneration();
    }
}

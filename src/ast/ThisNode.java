package ast;

import throwable.TypeException;
import throwable.UndecIDException;
import sem_util.SymbolTable;
import sem_util.STentry;
import type.Type;

import java.util.ArrayList;

public class ThisNode implements Node {

    private STentry entry;

    public ThisNode() {

    }

    @Override
    public ArrayList<String> checkSemantics(SymbolTable env) {
        ArrayList<String> res = new ArrayList<>();

        try {
            entry = env.processUse("this");
        } catch (UndecIDException e) {
            res.add(e.getMessage());
        }

        return res;
    }

    @Override
    public Type typeCheck() throws TypeException {
        return entry.getType();
    }

    @Override
    public String codeGeneration() {
        return "push " + entry.getOffset() + "\n"
                + "lfp\n"
                + "add\n"
                + "lw\n";
    }
}


package ast;

import throwable.TypeException;
import throwable.UndecIDException;
import parser.FOOLParser;
import sem_util.SymbolTable;
import type.ClassType;
import type.ObjectType;
import type.Type;
import sem_util.Field;

import java.util.ArrayList;

public class NewNode implements Node {

    private String classID;
    private ClassType classType;
    private ArrayList<Node> argumentsArrayList;
    private FOOLParser.NewExpContext newExpContext;

    public NewNode(String ID, ArrayList<Node> arguments, FOOLParser.NewExpContext ctx) {
        classID = ID;
        argumentsArrayList = arguments;
        newExpContext = ctx;
    }

    @Override
    public ArrayList<String> checkSemantics(SymbolTable env) {
        ArrayList<String> res = new ArrayList<>();

            try {
                try {//controllo se la classe è stata dichiarata
                    classType = (ClassType) env.getTypeOf(classID);
                } catch (Exception e1) {
                    throw new UndecIDException(classID);
                }
            //controllo se il numero di parametri è corretto
            if (classType.getFields().size() != argumentsArrayList.size())
                res.add("Instanziazione di una nuova classe " + classID + " con numero errato di parametri");

            //chiamo la checkSemantic su tutti gli argomenti
            if (argumentsArrayList.size() > 0) {
                for (Node node : argumentsArrayList)
                    res.addAll(node.checkSemantics(env));
            }
            env.processUse(classID);

        } catch (UndecIDException e) {
            res.add(e.getMessage());
        }
        return res;
    }

    @Override
    public Type typeCheck() throws TypeException {
        ArrayList<Field> fieldArrayList = classType.getFields();

        for (int i = 0; i < argumentsArrayList.size(); i++) {
            //controllo sul costruttore che i tipi dei parametri siano gli stessi dei tipi
            //dei campi
            Type argumentType = argumentsArrayList.get(i).typeCheck();
            Type fieldType = fieldArrayList.get(i).getType();
            if (!argumentType.isSubType(fieldType)) {
                throw new TypeException("Tipo errato per il parametro " + (i + 1) + " nell'invocazione del costruttore di " + classID, newExpContext);
            }
        }
        return new ObjectType(classType);
    }

    @Override
    public String codeGeneration() {
        //new pusha, in ordine, gli argomenti, il numero di argomenti e la label della classe
        StringBuilder argsCode = new StringBuilder();
        for (Node arg : argumentsArrayList) {
            argsCode.append(arg.codeGeneration());
        }
        return argsCode
                + "push " + argumentsArrayList.size() + "\n"
                + "push class" +classID + "\n"
                + "new\n";
    }
}

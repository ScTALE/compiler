package ast;

import throwable.TypeException;
import type.Type;
import sem_util.SymbolTable;
import vm_util.Label;

import java.util.ArrayList;

public class NotNode implements Node {

    private Node value;

    public NotNode(Node val) {
        value = val;
    }

    @Override
    public ArrayList<String> checkSemantics(SymbolTable env) {
        return new ArrayList<>();
    }


    @Override
    public Type typeCheck() throws TypeException {
        return value.typeCheck();
    }

    @Override
    public String codeGeneration() {
        String label = Label.nuovaLabel();
        String exit = Label.nuovaLabel();
        return value.codeGeneration() +
                "push 1\n" +
                "beq " + label + "\n" +
                "push 1\n" +
                "b " + exit + "\n" +
                label + ":\n" +
                "push 0\n" +
                exit + ":\n";
    }
}

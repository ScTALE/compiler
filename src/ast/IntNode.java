package ast;

import throwable.TypeException;
import sem_util.SymbolTable;
import type.IntType;
import type.Type;

import java.util.ArrayList;

public class IntNode implements Node {

    private int value;

    public IntNode(int val) {
        value = val;
    }

    @Override
    public ArrayList<String> checkSemantics(SymbolTable env) {
        return new ArrayList<>();
    }

    @Override
    public Type typeCheck() throws TypeException {
        return new IntType();
    }

    @Override
    public String codeGeneration() {
        return "push " + value + "\n";
    }
}

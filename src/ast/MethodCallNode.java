package ast;

import throwable.TypeException;
import throwable.UndecIDException;
import parser.FOOLParser;
import sem_util.SymbolTable;
import sem_util.STentry;
import type.ClassType;
import type.ArrowType;
import type.ObjectType;
import type.Type;

import java.util.ArrayList;

public class MethodCallNode extends FunCallNode {

    private String classID;
    private String methodID;
    private Type methodType;

    private int objectOffset;
    private int objectNestingLevel;
    private int methodOffset;
    private int nestinglevel;

    public MethodCallNode(String objID, String metID, ArrayList<Node> args, FOOLParser.MetExpContext metExpContext) {
        super(metID, args, metExpContext.funcall());
        classID = objID;
        methodID = metID;
    }

    @Override
    public ArrayList<String> checkSemantics(SymbolTable env) {
        ArrayList<String> res = new ArrayList<>();

        nestinglevel = env.getNestingLevel();

        //prendo l'offset e il tipo di oggetto e metodo e poi effettuo vari controlli

        try {
            ClassType classType = null;
            // vengono calcolati gli offset dalla dispatch table per avere
            // accesso all'oggetto del metodo
            if (classID.equals("this")) {
                Type objectType = env.getClassEntryforThis().getType();
                // se si sta utilizzando this ci si riferisce per forza alla classe corrente
                // perciò il valore di $fp è sempre 0
                objectOffset = 0;
                if (objectType instanceof ClassType) {
                    classType = (ClassType) objectType;
                    // l'oggetto si trova ad offset per 3, ovvero dopo l'indirizzo della dispatch
                    // table, il numero di parametri e i parametri
                    objectNestingLevel = 3;
                } else {
                    res.add("Impossibile chiamare this fuori da una classe");
                }
            } else {
                //prendo la STentry dell'oggetto dalla Symbol Table

                STentry objectSTentry = env.processUse(classID);
                Type objectType = objectSTentry.getType();
                objectOffset = objectSTentry.getOffset();
                objectNestingLevel = objectSTentry.getNestinglevel();
                try {
                    env.processUse("this");
                    // controllo se c'è this, se c'è sono in un metodo e decremento il nesting level
                    nestinglevel--;
                } catch (UndecIDException e) {

                }
                // check che il metodo sia stato invocato da un oggetto
                if (objectType instanceof ObjectType) {
                    classType = ((ObjectType) objectType).getClassType();
                } else {
                    res.add("Il metodo " + methodID + " è invocato da un tipo che non è un oggetto");
                    return res;
                }
            }

            // se il metodo è invocato con this allora significa che la classe a cui il metodo
            // appartiene è nell'ultima entry inserita nella SymbolTable

            STentry classEntry;
            if(classID.equals("this")) {
                classEntry=env.getClassEntryforThis();
            }
            else {
                classEntry=env.processUse(classType.getClassID());
            }

            ClassType objectClass = (ClassType) classEntry.getType();
            methodOffset = objectClass.getOffsetOfMethod(methodID);
            methodType = objectClass.getTypeOfMethod(methodID);

            // controllo che il metodo sia dichiarato all'interno della classe
            if (methodType == null) {
                res.add("L'oggetto " + classID + " non ha il metodo " + methodID);
            }

            ArrowType arrowType = (ArrowType) methodType;
            ArrayList<Type> arrowTypeArrayList = arrowType.getParametersTypeArrayList();
            //controllo che il metodo abbia il numero di parametri uguale a quello degli argomenti
            if (!(arrowTypeArrayList.size() == argumentsArrayList.size())) {
                res.add("Numero errato di parametri nell'invocazione di " + ID);
            }

            //CheckSemantic per ogni argomento
            for (Node node : argumentsArrayList)
                res.addAll(node.checkSemantics(env));

        } catch (UndecIDException e) {
            res.add(e.getMessage());
        }

        return res;
    }

    @Override
    public Type typeCheck() throws TypeException {
        ArrowType arrowType = (ArrowType) methodType;
        ArrayList<Type> arrowTypeArrayList = arrowType.getParametersTypeArrayList();

        for (int i = 0; i < argumentsArrayList.size(); i++)
            if (!argumentsArrayList.get(i).typeCheck().isSubType(arrowTypeArrayList.get(i)))
                throw new TypeException("Tipo errato per il parametro " + (i + 1) + " nell'invocazione di " + ID, getContext());

        return arrowType.getReturnType();
    }


    @Override
    public String codeGeneration() {
        StringBuilder parameterCode = new StringBuilder();
        for (int i = argumentsArrayList.size() - 1; i >= 0; i--)
            parameterCode.append(argumentsArrayList.get(i).codeGeneration());

        StringBuilder getActivationRecord = new StringBuilder();

        for (int i = 0; i < nestinglevel - objectNestingLevel; i++)
            getActivationRecord.append("lw\n");

        return "lfp\n"                                  // pusho frame pointer e parametri
                + parameterCode
                + "push " + objectOffset + "\n"         // pusho l'offset logico dell'oggetto (dispatch table)
                + "lfp\n"
                + getActivationRecord                                 //pusho access link (lw consecutivamente)
                // così si potrà risalire la catena statica
                + "add\n"                               // $fp + offset
                + "lw\n"                                // pusho indirizzo di memoria in cui si trova
                // l'indirizzo della dispatch table
                + "copy\n"                              // copio
                + "lw\n"                                // pusho l'indirizzo della dispatch table
                + "push " + (methodOffset - 1) + "\n"   // pusho l'offset di dove si trova metodo rispetto
                // all'inizio della dispatch table
                + "add" + "\n"                          // dispatch_table_start + offset
                + "loadc\n"                             // pusho il codice del metodo
                + "js\n";                               // jump all'istruzione dove e' definito il metodo e
                                                        // salvo $ra
    }
}

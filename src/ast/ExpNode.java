package ast;

import throwable.TypeException;
import parser.FOOLParser;
import sem_util.SymbolTable;
import type.IntType;
import type.Type;

import java.util.ArrayList;

//Nodo utilizzato per gestire Plus e Minus
public class ExpNode implements Node {
    private Node leftNode;
    private Node rightNode;
    private FOOLParser.ExpContext expContext;

    //identificativo per distinguere fra addizione e sottrazione
    private String ID;

    public ExpNode(Node l, Node r, FOOLParser.ExpContext context, String i) {
        leftNode = l;
        rightNode = r;
        expContext = context;
        ID = i;
    }

    @Override
    public ArrayList<String> checkSemantics(SymbolTable env) {

        ArrayList<String> res = new ArrayList<>();

        res.addAll(leftNode.checkSemantics(env));
        res.addAll(rightNode.checkSemantics(env));

        return res;
    }

    @Override
    public Type typeCheck() throws TypeException {
        if (!leftNode.typeCheck().isSubType(new IntType()) || !rightNode.typeCheck().isSubType(new IntType())) {
            throw new TypeException("Tipo incompatibile per " + ID + ". È richiesto un intero.", expContext);
        }
        return new IntType();
    }

    public String codeGeneration() {
        if (ID == "Plus")
        {
            return leftNode.codeGeneration() +
                    rightNode.codeGeneration() +
                    "add\n";
        }
        else
        {
            return leftNode.codeGeneration() +
                    rightNode.codeGeneration() +
                    "sub\n";
        }
    }
}

package ast;

import throwable.MultipleIDException;
import throwable.UndecIDException;
import org.antlr.v4.runtime.ParserRuleContext;
import sem_util.SymbolTable;
import sem_util.STentry;
import type.ClassType;
import type.ArrowType;
import type.ObjectType;
import type.Type;
import vm_util.FunctionCode;
import vm_util.Label;

import java.util.ArrayList;
import java.util.HashMap;

public class MethodNode extends FunNode {

    private String classID;

    public MethodNode(String ID, Type retType, ArrayList<ParameterNode> parametersArrayList, ArrayList<Node> declarationsArrayList, Node body, ParserRuleContext parserRuleContext) {
        super(ID, retType, parametersArrayList, declarationsArrayList, body, parserRuleContext);
    }

    public void setClassID( String ID ) { classID = ID; }

    @Override
    public ArrayList<String> checkSemantics(SymbolTable env) {

        ArrayList<String> res = new ArrayList();

        ArrayList<Type> parameterTypeArrayList = new ArrayList<>();

        for (ParameterNode parameterNode : parameterNodeArrayList) {
            parameterTypeArrayList.add(parameterNode.getType());
        }

        try {
            // Se restituisco un'istanza di una classe, aggiorno le informazioni
            if ( returnType instanceof ObjectType) {
                ObjectType objectType = (ObjectType) returnType;
                res.addAll(objectType.updateClassType(env));
            }
            env.processDeclaration(ID, new ArrowType(parameterTypeArrayList, returnType), env.getOffset());
            env.decreaseOffset();
        } catch (MultipleIDException e) {
            res.add("La funzione " + ID + " è già stata dichiarata");
        }

        //entro in un nuovo livello di scope
        HashMap<String, STentry> hm = new HashMap<>();
        env.pushHashMap(hm);

        //cerco la entry in cui è situata la classe
        try {
            STentry classEntry = env.processUse(classID);
            env.processDeclaration("this", new ObjectType((ClassType) classEntry.getType()), 0 );
        } catch (MultipleIDException | UndecIDException e) {
            e.printStackTrace();
        }

        //checkSemantic di tutti i parametri
        for (ParameterNode param : parameterNodeArrayList) {
            res.addAll(param.checkSemantics(env));
        }

        //checkSemantic di tutte le dichiarazioni interne al metodo
        if (declarationsArrayList.size() > 0) {
            env.setOffset(-2);
            for (Node n : declarationsArrayList)
                res.addAll(n.checkSemantics(env));
        }

        //checkSemantic del corpo del metodo
        res.addAll(body.checkSemantics(env));

        //esco dal livello di scope
        env.popHashMap();

        //ritorno eventuali errori rilevati
        return res;
    }

    @Override
    public String codeGeneration() {
        //variabili/funzioni dichiarate internamente
        StringBuilder localDeclarations = new StringBuilder();
        //variabili/funzioni da togliere dallo stack al termine del record di attivazione
        StringBuilder popLocalDeclarations = new StringBuilder();
        if (declarationsArrayList.size() > 0)
            for (Node dec : declarationsArrayList) {
                localDeclarations.append(dec.codeGeneration());
                popLocalDeclarations.append("pop\n");
            }
        //parametri in input da togliere dallo stack al termine del record di attivazione
        StringBuilder popInputParameters = new StringBuilder();
        for (Node dec : parameterNodeArrayList)
            popInputParameters.append("pop\n");

        String funLabel = Label.nuovaLabelPerFunzione();

        //inserisco il codice della funzione in fondo al main, davanti alla label
        FunctionCode.insertFunctionsCode(funLabel + ":\n" +
                "cfp\n" + //$fp diventa uguale al valore di $sp
                "lra\n" + //push return address
                localDeclarations + //push dichiarazioni locali
                body.codeGeneration() +
                "srv\n" + //pop del return value
                popLocalDeclarations +
                "sra\n" + // pop del return address
                "pop\n" + // pop dell'access link, per ritornare al vecchio livello di scope
                popInputParameters +
                "sfp\n" +  // $fp diventa uguale al valore del control link
                "lrv\n" + // push del risultato
                "lra\n" + // push del return address
                "js\n" // jump al return address per continuare dall'istruzione dopo
        );

        return funLabel + "\n";
    }

}
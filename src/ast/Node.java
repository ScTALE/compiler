package ast;

import java.util.ArrayList;

import throwable.TypeException;
import type.Type;
import sem_util.SymbolTable;

public interface Node {

  //fa il type checking e ritorna: 
  //  per una espressione, il suo tipo (oggetto BoolTypeNode o IntTypeNode)
  //  per una dichiarazione, "null"
  Type typeCheck() throws TypeException;
  
  String codeGeneration();
  
  ArrayList<String> checkSemantics(SymbolTable env);
  
}  
package ast;

import throwable.MultipleIDException;
import throwable.TypeException;
import parser.FOOLParser;
import sem_util.SymbolTable;
import type.Type;

import java.util.ArrayList;

public class ParameterNode implements Node {
    private String ID;
    private Type type;
    private int offset;
    private boolean insideClass;
    private FOOLParser.VardecContext vardecContext;

    public ParameterNode(String id, Type t, int o ,FOOLParser.VardecContext context) {
        ID = id;
        type = t;
        offset = o;
        insideClass = false;
        vardecContext = context;
    }

    public ParameterNode(String id, Type t, int o, boolean b,FOOLParser.VardecContext context) {
        ID = id;
        type = t;
        offset = o;
        insideClass = b;
        vardecContext = context;
    }

    public String getID() {
        return ID;
    }

    public Type getType() {
        return type;
    }

    @Override
    public ArrayList<String> checkSemantics(SymbolTable env) {
        ArrayList<String> ret = new ArrayList<>();
        try {
            env.processDeclarationforClass(ID, type, offset, insideClass);
        } catch (MultipleIDException e) {
            ret.add(e.getMessage());
        }
        return ret;
    }

    @Override
    public Type typeCheck() throws TypeException {
        return null;
    }

    @Override
    public String codeGeneration() {
        return "";
    }
}

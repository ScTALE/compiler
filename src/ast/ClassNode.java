package ast;

import throwable.*;
import sem_util.SymbolTable;
import sem_util.STentry;
import type.ClassType;
import type.ArrowType;
import type.ObjectType;
import type.Type;
import vm_util.DispatchTable;
import vm_util.DTentry;
import sem_util.Field;
import sem_util.Method;

import java.util.ArrayList;
import java.util.HashMap;

public class ClassNode implements Node {

    private String classID;
    private String superClassID;

    private HashMap<String, Type> fieldHashMap = new HashMap<>();
    private HashMap<String, ArrowType> methodHashMap = new HashMap<>();

    private ArrayList<ParameterNode> fieldDeclarationArrayList;
    private ArrayList<MethodNode> MethodDeclarationArrayList;

    private ClassType classType;

    public ClassNode(String ID, String superID, ArrayList<ParameterNode> fieldDec, ArrayList<MethodNode> metDec) {
        classID = ID;
        superClassID = superID;
        fieldDeclarationArrayList = fieldDec;
        MethodDeclarationArrayList = metDec;
    }

    public String getClassID() {
        return classID;
    }

    public String getSuperClassID() {
        return superClassID;
    }

    public ArrayList<ParameterNode> getFieldDeclarationArrayList() {
        return fieldDeclarationArrayList;
    }

    public ArrayList<MethodNode> getMethodDeclarationArrayList() {
        return MethodDeclarationArrayList;
    }

    @Override
    public ArrayList<String> checkSemantics(SymbolTable env) {

        ArrayList<String> res = new ArrayList<>();

        ArrayList<Field> fieldArrayList = new ArrayList<>();
        ArrayList<Method> methodArrayList = new ArrayList<>();

        //prende i campi passati in input e li inserisce in fieldArrayList per la STentry e in
        //fieldHashMap per accedervi velocemente

        for (ParameterNode parameterNode : fieldDeclarationArrayList) {
            fieldArrayList.add(new Field(parameterNode.getID(), parameterNode.getType()));
            fieldHashMap.put(parameterNode.getID(), parameterNode.getType());
        }

        for (MethodNode methodNode : MethodDeclarationArrayList) {
            ArrayList<Type> parameterTypeArrayList = new ArrayList<>();
            for (ParameterNode parameterNode : methodNode.getParameterNodeArrayList()) {
                // controlla i parametri di ogni metodo
                // se sono degli oggetti si controlla che siano definiti, altrimenti eccezione
                if (parameterNode.getType() instanceof ObjectType) {
                    ObjectType paramType = (ObjectType) parameterNode.getType();
                    String declaredClass = paramType.getClassType().getClassID();
                    try {
                        ClassType paramClassType = (ClassType) env.processUse(declaredClass).getType();
                        parameterTypeArrayList.add(new ObjectType(paramClassType));
                    } catch (UndecIDException e) {
                        res.add("La classe '" + declaredClass + " non è stata definita");
                    }
                } else //se i parametri sono "base", non oggetti
                    {
                    parameterTypeArrayList.add(parameterNode.getType());
                }
            }

            //prende i campi passati in input e li inserisce in methodArrayList per la STentry e in
            //methodHashMap per accedervi velocemente
            methodArrayList.add(new Method(methodNode.getID(), new ArrowType(parameterTypeArrayList, methodNode.getReturnType())));
            methodHashMap.put(methodNode.getID(), new ArrowType(parameterTypeArrayList, methodNode.getReturnType()));
        }


        ClassType superclassType;

        //controllo se la classe ha una superclasse per aggiornare correttamente
        //la Symbol Table

        try {
            superclassType = (ClassType) env.processUse(superClassID).getType();
        } catch (UndecIDException e) {
            superclassType = null;
        }

        // all'ID dichiarato si setta il tipo classe nella STentry
        try {
            classType = new ClassType(classID, superclassType, fieldArrayList, methodArrayList);
            env.setDeclarationType(classID, classType, 0);
        } catch ( UndecIDException e) {
            res.add(e.getMessage());
        }

        HashMap<String, STentry> hashMap = new HashMap<>();

        env.pushHashMap(hashMap);

        for (ParameterNode parameterNode : fieldDeclarationArrayList) {
            if (parameterNode.getType() instanceof ObjectType) {
                //sottoclassi non possono essere parametri di superclassi

                ClassType subClassAsParam = ((ObjectType) parameterNode.getType()).getClassType();

                if (subClassAsParam.isSubType(this.classType))
                    res.add("Non si può usare una sottoclasse nel costruttore della superclasse");
            }
            res.addAll(parameterNode.checkSemantics(env));
        }


        HashMap<String, STentry> hashmap2 = new HashMap<>();
        env.pushHashMap(hashmap2);

        //checkSemantic di ogni metodo
        for (MethodNode methodNode : MethodDeclarationArrayList) {
            res.addAll(methodNode.checkSemantics(env));
        }
        env.popHashMap();
        env.popHashMap();

        //controllo costruttore con superclasse

        if (!superClassID.isEmpty()) {
            try {
                //controllo che la classe che estende sia una classe
                if (!(env.getTypeOf(superClassID) instanceof ClassType))
                    res.add("L'ID della superclasse " + superClassID + " non è riferito a un tipo di classe");
            } catch (UndecIDException exp) {
                res.add("La superclasse " + superClassID + " non è definita");
            }

            try {
                ClassType superClassType = (ClassType) env.processUse(superClassID).getType();

                // controllo che il numero di attributi del costruttore sia uguale a quello della superclasse
                if (fieldDeclarationArrayList.size() >= superClassType.getFields().size()) {

                    for (int i = 0; i < superClassType.getFields().size(); i++) {
                        ParameterNode currentParameterNode = fieldDeclarationArrayList.get(i);
                        Field superClassField = superClassType.getFields().get(i);
                        //controllo che tipo e nome degli attributi siano uguali
                        if (!superClassField.getID().equals(currentParameterNode.getID())
                                || !currentParameterNode.getType().isSubType(superClassField.getType()) ) {
                            res.add("Il campo '" + currentParameterNode.getID() + "' della classe '"+ classID+"' fa override della superclasse con tipo differente");
                        }
                    }
                } else {
                    res.add("La sottoclasse non ha i parametri della superclasse");
                }
            } catch (UndecIDException e) {
                res.add("La superclasse " + superClassID + " non è definita " + e.getMessage());
            }

            try {
                //controllo ovveride se possibile

                //prendo entry e tipo della superclasse
                STentry superClassEntry = env.processUse(superClassID);
                ClassType superClassType = (ClassType) superClassEntry.getType();

                //se si trovano due metodi con lo stesso nome controllo che uno sia sottotipo dell'altro
                //altrimenti override non è compatibile
                HashMap<String, ArrowType> superClassMethodsHashMap = superClassType.getMethodsMap();
                for (String method : methodHashMap.keySet()) {
                    if (superClassMethodsHashMap.containsKey(method)) {
                        if (!methodHashMap.get(method).isSubType(superClassMethodsHashMap.get(method))) {
                            res.add("Override incompatibile del metodo '" + method + "' della classe '" + classID+"'");
                        }
                    }
                }

            } catch (UndecIDException e) {
                res.add("La superclasse " + superClassID + " non è definita " + e.getMessage());
            }
        }

        return res;
    }

    @Override
    public Type typeCheck() throws TypeException {

        //typecheck di ogni parametro e di ogni metodo

        for (ParameterNode parameterNode : fieldDeclarationArrayList){
            parameterNode.typeCheck();
        }

        for (MethodNode methodNode : MethodDeclarationArrayList) {
            methodNode.typeCheck();
        }

        return classType;
    }

    @Override
    public String codeGeneration() {

        ArrayList<DTentry> dispatchTable;
        // Creo una nuova dispatch table da zero se la classe non ha superclasse
        if(superClassID.equals("")) {
            dispatchTable=new ArrayList<>();
        }
        // Altrimenti la copio come base
        else {
            dispatchTable= DispatchTable.getDispatchTable(superClassID);
        }

        //contiene i metodi della superclasse
        HashMap<String, String> superClassMethodsHashMap = new HashMap<>();
        //aggiungo i metodi della superclasse alla hashmap
        for (DTentry d : dispatchTable) {
            superClassMethodsHashMap.put(d.getMethodID(), d.getMethodLabel());
        }
        //contiene i metodi della classe attuale
        HashMap<String, String> currentClassMethodsHashMap = new HashMap<>();
        //aggiungo i metodi della classe attuale
        for (MethodNode m : MethodDeclarationArrayList) {
            currentClassMethodsHashMap.put(m.getID(), m.codeGeneration());
        }
        //per ogni elemento della dispatch table:
        for (int i = 0; i < dispatchTable.size(); i++) {
            //gestione ovverride
            //prende il metodo dalla dispatch table, se presente
            String oldMethodID = dispatchTable.get(i).getMethodID();
            //lo sostituisce con il metodo proprio della classe
            String newMethodCode = currentClassMethodsHashMap.get(oldMethodID);
            //se l'ID esiste, vuol dire che è stato fatto override e la dispatch table viene aggiornata
            if (newMethodCode != null) {
                dispatchTable.set(i, new DTentry(oldMethodID, newMethodCode));
            }
        }
        //per ogni metodo:
        for (MethodNode m : MethodDeclarationArrayList) {
            //gestisce le funzioni aggiuntive della sottoclasse rispetto alla superclasse
            //contiene l'ID del metodo corrente
            String currentMethodID = m.getID();
            //se la superclasse non ha il metodo che si sta esaminando, lo si aggiunge alla dispatch table.
            if (superClassMethodsHashMap.get(currentMethodID) == null) {
                dispatchTable.add(new DTentry(currentMethodID, currentClassMethodsHashMap.get(currentMethodID)));
            }

        }

        //viene aggiunta la dispatch table corrispondente alla classe esaminata
        //questa operazione viene eseguita anche se la dispatch table è vuota
        //in quanto può capitare di implementare una classe senza metodi
        DispatchTable.addDispatchTable(classID, dispatchTable);

        return "";
    }
}
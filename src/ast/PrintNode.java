package ast;

import throwable.TypeException;
import sem_util.SymbolTable;
import type.Type;

import java.util.ArrayList;

public class PrintNode implements Node {
    private Node value;

    public PrintNode(Node val) {
        value = val;
    }

    @Override
    public ArrayList<String> checkSemantics(SymbolTable env) {
        return value.checkSemantics(env);
    }

    @Override
    public Type typeCheck() throws TypeException {
        return value.typeCheck();
    }

    @Override
    public String codeGeneration() {
        return value.codeGeneration() + "print\n";
    }
}

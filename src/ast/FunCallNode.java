package ast;

import throwable.TypeException;
import throwable.UndecIDException;
import parser.FOOLParser;
import sem_util.SymbolTable;
import type.ArrowType;
import type.Type;
import sem_util.STentry;

import java.util.ArrayList;

public class FunCallNode implements Node {
    protected String ID;
    protected ArrayList<Node> argumentsArrayList;
    protected STentry entry = null;
    protected int calledNestingLevel;
    private FOOLParser.FuncallContext funcallContext;

    public FunCallNode(String i, ArrayList<Node> a,FOOLParser.FuncallContext context) {
        ID = i;
        argumentsArrayList = a;
        funcallContext = context;
    }

    public FOOLParser.FuncallContext getContext(){
        return funcallContext;
    }

    @Override
    public ArrayList<String> checkSemantics(SymbolTable env) {
        ArrayList<String> res = new ArrayList<>();

        try {
            entry = env.processUse(ID);

            calledNestingLevel = env.getNestingLevel();

            //CheckSemantic di ogni argomento dato in input
            for (Node argument : argumentsArrayList)
                res.addAll(argument.checkSemantics(env));

        } catch (UndecIDException e) {
            res.add(ID + ": identificativo non definito");
        }


        return res;
    }

    @Override
    public Type typeCheck() throws TypeException {
        ArrowType arrowType;

        if (entry.getType().getID().equals(Type.ID.ARROW)) {
            arrowType = (ArrowType) entry.getType();
        } else {
            throw new TypeException("Invocazione di una non funzione " + ID, funcallContext);
        }

        ArrayList<Type> arrowTypeArrayList = arrowType.getParametersTypeArrayList();
        if (!(arrowTypeArrayList.size() == argumentsArrayList.size())) {
            throw new TypeException("Numero errato di parametri nell'invocazione di " + ID, funcallContext);
        }

        //Controllo che il tipo dei parametri sia lo stesso del tipo degli argomenti
        for (int i = 0; i < argumentsArrayList.size(); i++)
            if (!argumentsArrayList.get(i).typeCheck().isSubType(arrowTypeArrayList.get(i))) {
                throw new TypeException("Tipo errato per il parametro " + (i + 1) + " nell'invocazione di " + ID, funcallContext);
            }
        return arrowType.getReturnType();
    }

    @Override
    public String codeGeneration() {
        StringBuilder parameterCode = new StringBuilder();
        //parametri in ordine inverso
        for (int i = argumentsArrayList.size() - 1; i >= 0; i--)
            parameterCode.append(argumentsArrayList.get(i).codeGeneration());

        //utilizzato per gestire le funzioni annidate
        StringBuilder getActivationRecord = new StringBuilder();
        for (int i = 0; i < calledNestingLevel - entry.getNestinglevel(); i++)
            getActivationRecord.append("lw\n");

        return "lfp\n" + //pusho frame pointer e parametri
                parameterCode +
                "lfp\n" + getActivationRecord + //pusho access link (lw consecutivamente)
                // così si potrà risalire la catena statica
                "push " + entry.getOffset() + "\n" + // pusho l'offset logico per
                // accedere al codice della funzione
                "lfp\n" + getActivationRecord + //risalgo la catena statica
                "add\n" + //$fp + offset
                "lw\n" + //tramite l'indirizzo accedo al codice della funzione
                "js\n";
    }
}
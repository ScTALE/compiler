package ast;

import throwable.TypeException;
import parser.FOOLParser;
import sem_util.SymbolTable;
import type.BoolType;
import type.IntType;
import type.Type;
import vm_util.Label;

import java.util.ArrayList;

//Nodo utilizzato per gestire gli operatori logici
public class FactorNode implements Node {
    private Node leftNode;
    private Node rightNode;
    private FOOLParser.FactorContext factorContext;

    //identificativo per distinguere l'operatore logico
    private String ID;

    public FactorNode(Node l, Node r,FOOLParser.FactorContext context,String i) {
        leftNode = l;
        rightNode = r;
        factorContext = context;
        ID = i;
    }

    @Override
    public ArrayList<String> checkSemantics(SymbolTable env) {

        ArrayList<String> res = new ArrayList<>();

        res.addAll(leftNode.checkSemantics(env));
        res.addAll(rightNode.checkSemantics(env));

        return res;
    }

    @Override
    public Type typeCheck() throws TypeException {

        Type leftType = leftNode.typeCheck();
        Type rightType = rightNode.typeCheck();

        if(ID.equals("And")||(ID.equals("Or"))) {
            if (!leftType.isSubType(new BoolType()) || !rightType.isSubType(new BoolType())) {
                throw new TypeException("Tipo incompatibile per " + ID + ". È richiesto un booleano.", factorContext);
            }
        }
        else {
            if (!leftType.isSubType(new IntType()) || !rightType.isSubType(new IntType())) {
                throw new TypeException("Tipo incompatibile per " + ID + ". È richiesto un intero.", factorContext);
            }
        }

        return new BoolType();
    }

    @Override
    public String codeGeneration() {
        String label = Label.nuovaLabel();
        String exit = Label.nuovaLabel();
        switch (ID) {
            case "And":
                return  leftNode.codeGeneration()
                        + "push 0\n"
                        + "beq "+label+"\n"
                        + rightNode.codeGeneration()
                        + "push 0\n"
                        + "beq "+label+"\n"
                        + "push 1\n"
                        + "b "+exit+"\n"
                        + label + ":\n"
                        + "push 0\n"
                        + exit + ":\n";

            case "Or":
                return leftNode.codeGeneration()
                        + "push 1\n"
                        + "beq "+label+"\n"
                        + rightNode.codeGeneration()
                        + "push 1\n"
                        + "beq "+label+"\n"
                        + "push 0\n"
                        + "b "+exit+"\n"
                        + label + ":\n"
                        + "push 1\n"
                        + exit + ":\n";
            case "Eq":
                return leftNode.codeGeneration() +
                        rightNode.codeGeneration() +
                        "beq " + label + "\n" +
                        "push 0\n" +
                        "b " + exit + "\n" +
                        label + ":\n" +
                        "push 1\n" +
                        exit + ":\n";
            case "Greater":
                return rightNode.codeGeneration() +
                        "push 1\n" +
                        "add\n" +
                        leftNode.codeGeneration() +
                        "bleq " + label + "\n" +
                        "push 0\n" +
                        "b " + exit + "\n" +
                        label + ":\n" +
                        "push 1\n" +
                        exit + ":\n";
            case "GreaterEq":
                return rightNode.codeGeneration() +
                        leftNode.codeGeneration() +
                        "bleq " + label + "\n" +
                        "push 0\n" +
                        "b " + exit + "\n" +
                        label + ":\n" +
                        "push 1\n" +
                        exit + ":\n";
            case "Less":
                return leftNode.codeGeneration() +
                        "push 1\n" +
                        "add\n" +
                        rightNode.codeGeneration() +
                        "bleq " + label + "\n" +
                        "push 0\n" +
                        "b " + exit + "\n" +
                        label + ":\n" +
                        "push 1\n" +
                        exit + ":\n";
            case "LessEq":
                return leftNode.codeGeneration() +
                        rightNode.codeGeneration() +
                        "bleq " + label + "\n" +
                        "push 0\n" +
                        "b " + exit + "\n" +
                        label + ":\n" +
                        "push 1\n" +
                        exit + ":\n";
            default:
                return " ";
        }
    }
}
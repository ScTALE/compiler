import ast.Node;
import throwable.LexerException;
import throwable.ParserException;
import throwable.SemanticException;
import throwable.TypeException;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import parser.FOOLLexer;
import parser.FOOLParser;
import parser.SVMLexer;
import parser.SVMParser;
import sem_util.SymbolTable;
import util.FoolVisitorImpl;
import virtual_machine.ExecuteVM;
import vm_util.DispatchTable;
import vm_util.FunctionCode;
import vm_util.Label;

import java.io.*;
import java.util.ArrayList;

public class FullTest {
    public static void main(String[] args)  {

        File file = new File("src/test");
        String[] names = file.list();
        int totale = 0;
        int corretto = 0;
        String expectedResult="";
        String message = "No output";

        for(String name : names) {
            try {
                String fileName = "src/test/"+name;
                System.out.println(name + "\n");
                CharStream input = CharStreams.fromFileName(fileName);
                totale++;
                //separatore per risultato
                String sCurrentLine;
                String lastLine="";


                BufferedReader br = new BufferedReader(new FileReader(fileName));

                while ((sCurrentLine = br.readLine()) != null) {
                    lastLine = sCurrentLine;
                }
                expectedResult=lastLine.replace("//","");

                FOOLLexer lexer = new FOOLLexer(input);
                CommonTokenStream tokens = new CommonTokenStream(lexer);

                if (lexer.lexicalErrors.size() > 0) {
                    throw new LexerException(lexer.lexicalErrors);
                }

                FOOLParser parser = new FOOLParser(tokens);
                FOOLParser.ProgContext progContext = parser.prog(); //parser.prog riutilizzato

                if (parser.getNumberOfSyntaxErrors() > 0) {
                    throw new ParserException("Errori rilevati: " + parser.getNumberOfSyntaxErrors() + "\n");
                }

                FoolVisitorImpl visitor = new FoolVisitorImpl();

                Node ast = visitor.visit(progContext); //generazione AST

                SymbolTable env = new SymbolTable();
                ArrayList<String> err = ast.checkSemantics(env);

                if (err.size() > 0) {
                    throw new SemanticException(err);
                }

                ast.typeCheck(); //type-checking bottom-up, ritorna un type

                // CODE GENERATION
                String code = ast.codeGeneration();
                code += DispatchTable.generaCodiceDispatchTable();

                File svmFile = new File("codice.svm");
                BufferedWriter svmWriter = new BufferedWriter(new FileWriter(svmFile.getAbsoluteFile()));
                svmWriter.write(code);
                svmWriter.close();

                CharStream inputASM = CharStreams.fromFileName(svmFile.getAbsolutePath());
                SVMLexer lexerASM = new SVMLexer(inputASM);
                CommonTokenStream tokensASM = new CommonTokenStream(lexerASM);
                SVMParser parserASM = new SVMParser(tokensASM);

                parserASM.assembly();

                if (lexerASM.errors.size() > 0) {
                    throw new LexerException(lexerASM.errors);
                }
                if (parserASM.getNumberOfSyntaxErrors() > 0) {
                    throw new ParserException("Errore di parsing in SVM");
                }

                int[] bytecode = parserASM.getBytecode();
                //cancello il file di output
                svmFile.delete();
                ExecuteVM vm = new ExecuteVM(bytecode);
                ArrayList<String> out = vm.cpu();
                if (out.size() > 0)
                    message = out.get(out.size() - 1);
                if (expectedResult.equals(message)) {
                    System.out.println("OK\n");
                    corretto++;
                }
                else {
                    System.out.println("Risultato aspettato: " + expectedResult);
                    System.out.println("Risultato ottenuto: " + message + "\n");
                    System.out.println("Fallito\n");
                }
                Label.reset();
                FunctionCode.reset();
                DispatchTable.reset();
            }
            catch (LexerException | IOException | SemanticException | TypeException | ParserException e) {
                message = e.getMessage();

                if (expectedResult.equals(message)) {
                    System.out.println("OK\n");
                    corretto++;
                }
                else {
                    System.out.println("Risultato aspettato: " + expectedResult);
                    System.out.println("Risultato ottenuto: " + message + "\n");
                    System.out.println("Fallito\n");
                }
            }
        }
        if(corretto==totale)System.out.println("Test corretti: " + corretto + "/" + totale + ".");
        else System.out.println("Test corretti: " + corretto + "/" + totale + ".");
    }
}
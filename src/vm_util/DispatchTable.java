package vm_util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DispatchTable {

    //Hashmap di tutte le dispatch table
    private static HashMap<String, ArrayList<DTentry>> dispatchTables = new HashMap<>();

    public static void addDispatchTable(String classID, ArrayList<DTentry> dt) {
        dispatchTables.put(classID, dt);
    }

    // Viene ritornata una copia della dispatch table (così non si modifica, per riferimento, quella del padre)
    public static ArrayList<DTentry> getDispatchTable(String classID) {
        ArrayList<DTentry> tmp = new ArrayList<>();
        ArrayList<DTentry> dt = dispatchTables.get(classID);
        for (DTentry dte : dt) {
            DTentry tmp2 = new DTentry(dte.getMethodID(), dte.getMethodLabel());
            tmp.add(tmp2);
        }
        return tmp;
    }

    public static String generaCodiceDispatchTable() {
        StringBuilder stringBuilder = new StringBuilder();
        // String è l'ID della classe, con un'arraylist di tutti i suoi metodi
        // Per ogni classe
        for (Map.Entry<String, ArrayList<DTentry>> dt : dispatchTables.entrySet()) {
            //Genera le label delle classi
            stringBuilder.append("class" + dt.getKey() + ":\n"); // nuovaLabelPerDispatchTable
            // Per ogni elemento della dispatch table
            for (DTentry entry : dispatchTables.get(dt.getKey())) {
                //Aggiunge sotto alla label della classe le label delle funzioni a cui si riferisce
                stringBuilder.append(entry.getMethodLabel());
            }
        }
        return stringBuilder.toString();
    }
    public static void reset() {
        dispatchTables = new HashMap<>();
    }
}
package util;

import java.util.ArrayList;

import ast.*;
import throwable.TypeException;
import parser.*;
import parser.FOOLParser.BaseExpContext;
import parser.FOOLParser.BoolValContext;
import parser.FOOLParser.DecContext;
import parser.FOOLParser.ExpContext;
import parser.FOOLParser.FactorContext;
import parser.FOOLParser.FunContext;
import parser.FOOLParser.FunExpContext;
import parser.FOOLParser.IfExpContext;
import parser.FOOLParser.IntValContext;
import parser.FOOLParser.LetInExpContext;
import parser.FOOLParser.SingleExpContext;
import parser.FOOLParser.TermContext;
import parser.FOOLParser.TypeContext;
import parser.FOOLParser.VarExpContext;
import parser.FOOLParser.VarasmContext;
import parser.FOOLParser.VardecContext;
import parser.FOOLParser.MetContext;
import type.Type;

public class FoolVisitorImpl extends FOOLBaseVisitor<Node> {

	@Override
	public Node visitLetInExp(LetInExpContext letInExpContext) {

		//resulting node of the right type
		LetInNode res;

		//list of declarationsArrayList in @res
		ArrayList<Node> declarations = new ArrayList<Node>();

		//visit all nodes corresponding to declarationsArrayList inside the let context and store them in @declarationsArrayList
		//notice that the ctx.let().dec() method returns a list, this is because of the use of * or + in the grammar
		//antlr detects this is a group and therefore returns a list
		for(DecContext dc : letInExpContext.let().dec()){
			declarations.add( visit(dc) );
		}

		//visit exp context
		Node exp = visit( letInExpContext.exp() );

		//build @res accordingly with the result of the visits to its content
		res = new LetInNode(declarations, exp);

		return res;
	}

	@Override
	public Node visitSingleExp(SingleExpContext singleExpContext) {

		//serve per la singola espressione print *qualcosa*
		return new SingleExpNode(visit(singleExpContext.exp()));

	}


	@Override
	public Node visitVarasm(VarasmContext varasmContext) {
		//dichiarazione di variabile ed assegnamento
		//visit the type
		Type typeNode;
		try {
			typeNode = visit(varasmContext.vardec().type()).typeCheck();
		} catch (TypeException e) {
			return null;
		}
		//visit the exp
		Node expNode = visit(varasmContext.exp());

		//build the varNode
		return new VarNode(varasmContext.vardec().ID().getText(), typeNode, expNode,varasmContext);
	}

	@Override
	public Node visitFun(FunContext funContext) {

		try {
			// initialize @res with the visits to the type and its ID
			ArrayList<ParameterNode> params = new ArrayList<>();

			// add argument declarationsArrayList
			// we are getting a shortcut here by constructing directly the ParameterNode
			// this could be done differently by visiting instead the VardecContext
			for (int i = 0; i < funContext.vardec().size(); i++) {
				VardecContext vc = funContext.vardec().get(i);
				params.add(new ParameterNode(vc.ID().getText(), visit(vc.type()).typeCheck(), i + 1, vc));
			}

			// add body, create a list for the nested declarationsArrayList
			ArrayList<Node> declarations = new ArrayList<Node>();
			// check whether there are actually nested decs
			if (funContext.let() != null) {
				// if there are visit each dec and add it to the @innerDec list
				for (DecContext dc : funContext.let().dec())
					declarations.add(visit(dc));
			}

			// get the exp body
			Node body = visit(funContext.exp());

			return new FunNode(funContext.ID().getText(), visit(funContext.type()).typeCheck(), params, declarations, body, funContext);
		} catch (TypeException e) {
			return null;
		}

	}

	@Override
	public Node visitType(TypeContext typeContext) {
		//tutti i tipi gestiti da TypeNode
		return new TypeNode(typeContext.getText());
	}

	@Override
	public Node visitExp(ExpContext expContext) {

		//check whether this is a simple or binary expression
		//notice here the necessity of having named elements in the grammar
		if (expContext.right == null) {
			//it is a simple expression
			return visit(expContext.left);
		} else {
			//it is a binary expression, you should visit left and right
			if (expContext.operator.getType() == FOOLLexer.PLUS) {
				return new ExpNode(visit(expContext.left), visit(expContext.right), expContext,"Plus");
			} else {
				return new ExpNode(visit(expContext.left), visit(expContext.right), expContext,"Minus");
			}
		}

	}

	@Override
	public Node visitTerm(TermContext termContext) {
		//check whether this is a simple or binary expression
		//notice here the necessity of having named elements in the grammar
		if (termContext.right == null) {
			//it is a simple expression
			return visit(termContext.left);
		} else {
			//it is a binary expression, you should visit left and right
			if (termContext.operator.getType() == FOOLLexer.TIMES) {
				return new TermNode(visit(termContext.left), visit(termContext.right),termContext,"mult");

			} else {
				return new TermNode(visit(termContext.left), visit(termContext.right),termContext,"div");
			}
		}
	}


	@Override
	public Node visitFactor(FactorContext factorContext) {
		//check whether this is a simple or binary expression
		//notice here the necessity of having named elements in the grammar
		try {
			if (factorContext.right == null) {
				//it is a simple expression
				return visit(factorContext.left);
			} else {
				//it is a binary expression, you should visit left and right
				switch (factorContext.operator.getType()) {
					case FOOLLexer.AND:
						return new FactorNode(visit(factorContext.left), visit(factorContext.right),factorContext,"And");
					case FOOLLexer.OR:
						return new FactorNode(visit(factorContext.left), visit(factorContext.right),factorContext,"Or");
					case FOOLLexer.EQ:
						return new FactorNode(visit(factorContext.left), visit(factorContext.right), factorContext,"Eq");
					case FOOLLexer.GREATER:
						return new FactorNode(visit(factorContext.left), visit(factorContext.right),factorContext,"Greater");
					case FOOLLexer.GREATEREQ:
						return new FactorNode(visit(factorContext.left), visit(factorContext.right), factorContext,"GreaterEq");
					case FOOLLexer.LESS:
						return new FactorNode(visit(factorContext.left), visit(factorContext.right),factorContext,"Less");
					case FOOLLexer.LESSEQ:
						return new FactorNode(visit(factorContext.left), visit(factorContext.right), factorContext,"LessEq");
					default:
						throw new Exception("Operatore non valido");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}


	@Override
	public Node visitIntVal(IntValContext ctx) {
		if(ctx.MINUS()==null)
			return new IntNode(Integer.parseInt(ctx.INTEGER().getText()));
		else //gestiamo il caso di numeri negativi
			return new IntNode(-Integer.parseInt(ctx.INTEGER().getText()));
	}

	@Override
	public Node visitBoolVal(BoolValContext ctx) {

		String text = ctx.getText().replace("!", "");
		//gestiamo il caso del not
		if (ctx.NOT() == null) {
			return new BoolNode(Boolean.parseBoolean(text));
		} else {
			return new NotNode(new BoolNode(Boolean.parseBoolean(text)));
		}
	}

	@Override
	public Node visitBaseExp(BaseExpContext ctx) {
		return visit (ctx.exp());
	}

	@Override
	public Node visitIfExp(IfExpContext ctx) {

		//create the resulting node
		IfNode res;

		//visit the conditional, then the then branch, and then the else branch
		//notice once again the need of named terminals in the rule, this is because
		//we need to point to the right expression among the 3 possible ones in the rule

		Node condExp = visit (ctx.cond);

		Node thenExp = visit (ctx.thenBranch);

		Node elseExp = visit (ctx.elseBranch);

		//build the @res properly and return it
		res = new IfNode(condExp, thenExp, elseExp, ctx);

		return res;
	}

	@Override
	public Node visitVarExp(VarExpContext ctx) {

		if(ctx.MINUS()==null)
			return new IdNode(ctx.ID().getText(),ctx,false);
		else //gestisco il caso di variabili negative (es. -x)
			return new IdNode(ctx.ID().getText(),ctx,true);

	}

	@Override
	public Node visitPrint(FOOLParser.PrintContext ctx) {

		return new PrintNode(visit(ctx.exp()));

	}

	@Override
	public Node visitFunExp(FunExpContext ctx) {
		//funzione dichiarata
		return visit(ctx.funcall());
	}
	@Override
	public Node visitFuncall(FOOLParser.FuncallContext funcallContext) {
		//old prof. implementation of FunEXp
		Node res;
		String functionID;
		//get the invocation argumentsArrayList
		ArrayList<Node> args = new ArrayList<Node>();

		for (ExpContext exp : funcallContext.exp())
			args.add(visit(exp));

		functionID = funcallContext.ID().getText();

        res = new FunCallNode(functionID, args, funcallContext);

		return res;
	}

	//---------------------
	//PARTE OBJECT ORIENTED
	//---------------------

    @Override
    public Node visitClassExp(FOOLParser.ClassExpContext classExpContext) {

        ClassDecNode res;

        try {
        	//creo 3 ArrayList con tutte le classi, i parametri e i metodi
            ArrayList<ClassNode> classDeclarations = new ArrayList<>();
            //per ogni dichiarazione di classe
            for (FOOLParser.ClassdecContext classdecContext : classExpContext.classdec()) {
                ArrayList<ParameterNode> parameterNodeArrayList = new ArrayList<>();

                //per ogni parametro
                for (int i = 0; i < classdecContext.vardec().size(); i++) {
                    VardecContext vardecContext = classdecContext.vardec().get(i);
					parameterNodeArrayList.add(new ParameterNode(vardecContext.ID().getText(), visit(vardecContext.type()).typeCheck(), i + 1, true, vardecContext));
                }
                ArrayList<MethodNode> methodNodeArrayList = new ArrayList<>();

                //per ogni metodo
                for (MetContext metContext : classdecContext.met()) {
                    MethodNode method = (MethodNode) visit(metContext);
                    method.setClassID(classdecContext.ID(0).getText());
                    methodNodeArrayList.add(method);
                }

                ClassNode classNode;
                //se è null significa che la classe non estende nulla
                if (classdecContext.ID(1) == null) {
                    classNode = new ClassNode(classdecContext.ID(0).getText(), "", parameterNodeArrayList, methodNodeArrayList);
                } else { //altrimenti ha una superclasse
                    classNode = new ClassNode(classdecContext.ID(0).getText(), classdecContext.ID().get(1).getText(), parameterNodeArrayList, methodNodeArrayList);
                }
                classDeclarations.add(classNode);
            }

            //nel caso in cui la dichiarazione di classi non sia seguita da una SingleExp
            if (classExpContext.let() != null) {
                ArrayList<Node> letDeclarations = new ArrayList<>();
                for (DecContext dc : classExpContext.let().dec()) {
                    letDeclarations.add(visit(dc));
                }
                Node exp = visit(classExpContext.exp());
                res = new ClassDecNode(classDeclarations, new LetInNode(letDeclarations, exp));
            }
            else {
                Node exp = visit(classExpContext.exp());
                res = new ClassDecNode(classDeclarations, new LetInNode(new ArrayList<>() , exp));
            }
        } catch (TypeException e) {
            return null;
        }


        return res;
    }


    @Override
	public Node visitMet(FOOLParser.MetContext metContext) {
		try {
			FunContext funContext = metContext.fun();

			ArrayList<ParameterNode> parameterNodeArrayList = new ArrayList<>();

			//gestisco i parametri
			for (int i = 0; i < funContext.vardec().size(); i++) {
				VardecContext vardecContext = funContext.vardec().get(i);
				parameterNodeArrayList.add(new ParameterNode(vardecContext.ID().getText(), visit(vardecContext.type()).typeCheck(), i + 1, vardecContext));
			}

			// arraylist per le dichiarazioni annidate
			ArrayList<Node> nestedDeclarations = new ArrayList<>();
			// controlla dichiarazioni annidate
			if (funContext.let() != null) {
				for (DecContext decContext : funContext.let().dec())
					nestedDeclarations.add(visit(decContext));
			}

			// visita il corpo della funzione e lo ritorna
			Node body = visit(funContext.exp());
			return new MethodNode(funContext.ID().getText(), visit(funContext.type()).typeCheck(), parameterNodeArrayList, nestedDeclarations, body, funContext);

		}
		catch (TypeException e) {
			return null;
		}
	}

    public Node visitMetExp(FOOLParser.MetExpContext metExpContext) {

		ArrayList<Node> arguments = new ArrayList<>();

		//gestione argomenti dati come input al metodo
        for (ExpContext exp : metExpContext.funcall().exp()) {
            arguments.add(visit(exp));
        }

        String methodID = metExpContext.funcall().ID().getText();
		String objectID;

		//o si fa riferimento alla classe con this, o con l'ID della classe
		if (metExpContext.THIS() != null) {
			objectID=metExpContext.THIS().getText();
		}
		else {
			objectID=metExpContext.ID().getText();
		}

        return new MethodCallNode(objectID, methodID, arguments, metExpContext);
    }

    @Override
    public Node visitNewExp(FOOLParser.NewExpContext newExpContext) {

        NewNode res;
        String classID;

        //argumenti per il costruttore della classe
        ArrayList<Node> arguments = new ArrayList<>();

        //ID della classe che si vuole istanziare
        classID = newExpContext.ID().getText();

        for (ExpContext exp : newExpContext.exp()) {
            arguments.add(visit(exp));
        }

        res = new NewNode(classID, arguments, newExpContext);

        return res;
    }

	@Override
	public Node visitThisExp(FOOLParser.ThisExpContext thisExpContext) {
		return new ThisNode();
	}

}

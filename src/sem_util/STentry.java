package sem_util;

import type.Type;

public class STentry {
 
    private int nestingLevel; //nestinglevel
    private Type type;
    private int offset;
    private boolean insideClass;

    public STentry (int n, int os)  {
        nestingLevel=n;
        offset=os;
    }
   
    public STentry (int n, Type t, int os)  {
        nestingLevel=n;
        type=t;
        offset=os;
    }

    public STentry(int n, Type t, int os, boolean b) {
        nestingLevel = n;
        type = t;
        offset = os;
        insideClass = b;
    }
  
    public Type getType()  {
        return type;
    }

    public int getOffset()  {
        return offset;
    }

    public boolean getInsideClass() {return insideClass;}

    public int getNestinglevel()  {
        return nestingLevel;
    }
}  
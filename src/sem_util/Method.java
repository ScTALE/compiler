package sem_util;

import type.ArrowType;

public class Method {

    private String id;
    private ArrowType type;

    public Method(String i, ArrowType t) {
        id = i;
        type = t;
    }

    public String getId() {
        return id;
    }

    public ArrowType getType() {
        return type;
    }
}
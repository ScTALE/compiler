package sem_util;

import java.util.LinkedList;
import java.util.HashMap;
import java.util.ListIterator;

import throwable.*;
import type.Type;
import type.ClassType;
import type.ArrowType;

public class SymbolTable {

    //implementazione con lista di HashTable
	private LinkedList<HashMap<String, STentry>>  symTable = new LinkedList<>();

	//offset della entry rispetto all'area di memoria in cui è definita
	private int offset = 0;

	//variabile che memorizza la entry dell'ultima classe, in modo tale da potervi accedere con this
    private STentry classEntryforThis = null;

    //Il Nesting Level è ottenibile semplicemente così
    public int getNestingLevel() {
        return symTable.size() - 1;
    }

    public int getOffset(){ return offset; }
    public void setOffset(int n){ offset = n;}
    public void increaseOffset(){ offset = offset + 1;}
    public void decreaseOffset(){ offset = offset - 1;}

    public LinkedList<HashMap<String,STentry>> getSymtable() { return symTable; }

    public STentry getClassEntryforThis() {
        return classEntryforThis;
    }

    public Type getTypeOf(String id) throws UndecIDException {
        return processUse(id).getType();
    }

    //scope entry: si aggiunge un nuovo livello di scope
    public void pushHashMap(HashMap hm){
		symTable.add(hm);
	}

    //on scope exit: si rimuove il livello di scope più esterno, ovvero l'ultima HashMap aggiunta
    public void popHashMap(){
        symTable.remove(getNestingLevel());
    }

    //aggiunge nell'hashmap piú esterna la coppia (ID, STentry) dove la STentry è composta da
    //un tipo e un offset. Se l'ID è già presente, significa che l'identificativo è già
    // definito e viene lanciata l'eccezione MultipleIDException
    public SymbolTable processDeclaration(String id, Type type, int offset) throws MultipleIDException {
        STentry nuovaEntry = new STentry(getNestingLevel(), type, offset);
        checkProcessDeclaration(nuovaEntry,id,type);
        return this;
    }

    //come processDeclaration ma utilizzata specificatamente per le classi
	public SymbolTable processDeclarationforClass(String id, Type type, int offset, boolean inside) throws MultipleIDException {
		STentry nuovaEntry = new STentry(getNestingLevel(), type, offset, inside);
		checkProcessDeclaration(nuovaEntry,id,type);
		return this;
	}

    //aggiorna l'attributo type della STentry con chiave ID
    //è utilizzato per aggiornare il supertipo delle classi (dopo tutte le classdec)
	public SymbolTable setDeclarationType(String id, Type newtype, int offset) throws UndecIDException {
		STentry nuovaEntry = new STentry(getNestingLevel(), newtype, offset);
		STentry  vecchiaEntry = symTable.get(getNestingLevel()).replace(id, nuovaEntry);
		if (newtype instanceof ClassType) {
            classEntryforThis = nuovaEntry;
		}
		if (vecchiaEntry == null) {
			throw new UndecIDException(id);
		}
		return this;
	}

    //scorre la lista di hashtable e cerca la STentry con chiave ID
    //se non presente, l'ID non è definito e viene lanciata l'eccezione
    public STentry processUse(String id) throws UndecIDException {
        ListIterator<HashMap<String, STentry>> li = symTable.listIterator(symTable.size());
        while (li.hasPrevious()) {
            HashMap<String, STentry> current = li.previous();
            if (current.containsKey(id)) {
                return current.get(id);
            }
        }
        throw new UndecIDException(id);
    }

    //uguale a processUse ma ignora le entry di tipo funzione
    public STentry processUseIgnoreArrow(String id) throws UndecIDException {
        ListIterator<HashMap<String, STentry>> li = symTable.listIterator(symTable.size());
        while (li.hasPrevious()) {
            HashMap<String, STentry> current = li.previous();
            if (current.containsKey(id) && !(current.get(id).getType() instanceof ArrowType)) {
                return current.get(id);
            }
        }
        throw new UndecIDException(id);
    }

    //funzione ausiliaria per processDeclaration
    private void checkProcessDeclaration(STentry nuovaEntry, String id, Type type)throws MultipleIDException{
        STentry vecchiaEntry = symTable
                .get(this.symTable.size() - 1)
                .put(id, nuovaEntry);
        if (type instanceof ClassType) {
            classEntryforThis = nuovaEntry;
        }
        if (vecchiaEntry != null) {
            throw new MultipleIDException(id);
        }
    }
}

package type;

import java.util.ArrayList;

public class ArrowType implements Type {

    private ArrayList<Type> parametersTypeArrayList;
    private Type returnType;

    public ArrowType(ArrayList<Type> p, Type r) {
        parametersTypeArrayList = p;
        returnType = r;
    }

    @Override
    public ID getID() {
        return ID.ARROW;
    }

    public ArrayList<Type> getParametersTypeArrayList() {
        return parametersTypeArrayList;
    }

    public Type getReturnType() {
        return returnType;
    }

    /*Il tipo di una funzione f1 e` sottotipo del tipo di una funzione f2 se il tipo ritornato da f1
    e` sottotipo del tipo ritornato da f2, se hanno il medesimo numero di parametri,
    e se ogni tipo di paramentro di f1 e` sopratipo del corrisponde tipo di parametro di f2. */

    @Override
    public boolean isSubType(Type t)
    {
        if (t instanceof ArrowType)
        {
            ArrowType arrowType = (ArrowType) t;
            boolean controllo = true;
            if (parametersTypeArrayList.size() == arrowType.getParametersTypeArrayList().size())
            {
                //Controllo che tutti i parametri abbiano lo stesso tipo(supertype, come da cosegna)
                for (int i = 0; i < parametersTypeArrayList.size(); i++)
                {
                    controllo = controllo & (arrowType.getParametersTypeArrayList().get(i).isSubType(parametersTypeArrayList.get(i)));
                }
                //Controllo che anche il valore di ritorno della funzione
                controllo = controllo & returnType.isSubType(arrowType.getReturnType());
            }
            else
            {
                controllo = false;
            }
            return controllo;
        }
        else
        {
            return false;
        }
    }

    @Override
    public String toPrint()
    {
        return "fun";
    }
}

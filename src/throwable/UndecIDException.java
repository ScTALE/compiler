package throwable;

public class UndecIDException extends Exception {
    public UndecIDException(String ID) { super(ID + ": identificativo non dichiarato."); }
}
